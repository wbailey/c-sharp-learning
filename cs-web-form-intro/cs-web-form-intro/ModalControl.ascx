﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalControl.ascx.cs" Inherits="cs_web_form_intro.ModalControl" %>

<!-- Style for the modal -->
<style>
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto; /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
        width: 80%; /* Could be more or less, depending on screen size */
    }
    .close {
        color: #aaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }
</style>

<!-- HTML for the modal -->
<div id="<%= ModalID %>" class="modal">
    <div class="modal-content">
        <button 
            id="<%= ModalID %>_closeButton"
            type="button">
                &times;
        </button>
        <p>Some text in the Modal..</p>
    </div>
</div>
<asp:Button 
    type="button"
    ID="OpenModalButton" 
    runat="server" 
    Text="Open Modal" 
    CausesValidation="false" />

<!-- JavaScript for the modal -->
<script>
    function <%= ModalID %>_openModal() {
        try {
            const modalDiv = document.getElementById("<%= ModalID %>");
            const modalCloseButton = document.getElementById("<%= ModalID %>_closeButton");
            modalDiv.style.display = 'block';

            modalCloseButton.onclick = function () {
                modalDiv.style.display = 'none';
            };
            console.log("hello from the modal! This modal's id is: <%= ModalID %>");
        } catch (e) {
            alert(e);
        }
    }
</script>
