using Model;
using Controller;

namespace View
{
    // private methods to draw ASCII prompts
    class Draw 
    {
        public static void AnyKey()
        {
            Console.WriteLine("Press Any Key to Continue...");
        }

        public static void Title()
        {
            Console.WriteLine("Welcome to C# TicTacToe!");

        }

        public static void TopMessage(State state)
        {
            Console.WriteLine($"{state.TopMessage}");
        }

        public static void Board(State state)
        {
            Console.WriteLine($"  1 2 3");
            char[] rowLabels = { 'A', 'B', 'C' };
            Func<string, string> replaceDash = square => square == "_" ? " " : square;

            for (int i = 0; i < 3; i++)
            {
                if (i < 2) 
                {
                    Console.WriteLine($"{rowLabels[i]} {state.GameBoard[i, 0]}|{state.GameBoard[i, 1]}|{state.GameBoard[i, 2]}");
                }
                else 
                {
                    Console.WriteLine($"{rowLabels[i]} {replaceDash(state.GameBoard[i, 0])}|{replaceDash(state.GameBoard[i, 1])}|{replaceDash(state.GameBoard[i, 2])}");
                }
            }
        }

        public static void CurrentPlayer(State state)
        {
            Console.WriteLine($"Player \"{state.CurrentPlayer}\" it is your turn.");
        }

        public static void Info()
        {
            Console.WriteLine("Indicate what square to make your move in or \"i\" for information on how to play");
        }

        public static void InputPrompt()
        {
            Console.WriteLine("Make your Selection: ");
            Console.Write("> ");
        }

        public static void DebugCommands()
        {
            string[] commands = {"[i]sactive","[t]opmessage", "[c]urrentplayer", "[g]ameboard", "[e]xit, [w]in" };
            Console.WriteLine("The following debug commands are available:");
            for (int i = 0; i < commands.Length; i++) 
            {
                Console.Write($"{commands[i]} ");
            }
            Console.Write("\n");
        }

        public static void DebugGameBoard(State state) 
        {
            Console.WriteLine($"  1 2 3");
            char[] rowLabels = { 'A', 'B', 'C' };

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine($"{rowLabels[i]} {state.GameBoard[i, 0]}|{state.GameBoard[i, 1]}|{state.GameBoard[i, 2]}");
            }
        }
    }

    // Rendering methods analogous to program states
    public class Render()
    {
        public static void Title()
        {
            Console.Clear();
            Draw.Title();
            Draw.AnyKey();
        }

        public static void GamePlay(State state)
        {
            Console.Clear();
            Draw.TopMessage(state);
            Draw.Board(state);
            Draw.CurrentPlayer(state);
            Draw.Info();
            Draw.InputPrompt();
        }
        
        public static void Win(State state)
        {
            Console.Clear();
            Console.WriteLine($"Player {state.CurrentPlayer} has won!");
            Draw.AnyKey();
        }

        public static void Info(State state) 
        {
            Console.Clear();
            Console.WriteLine("C# Tic Tac Toe");
            Console.WriteLine("Two player Tic Tac Toe.");
            Console.WriteLine("Players alternate making moves using the letter row labels A-C and number column labels 1-3 in the following format:");
            Console.WriteLine("\"A1\": Current Player makes move in upper left corner.");
            Console.WriteLine("\"C3\": Current Player makes move in lower right corner.");
            Console.WriteLine("Additional Commands:");
            Console.WriteLine("\"e\": exit game to command prompt.");
            Console.WriteLine("\"i\": show this screen");
            Console.WriteLine("\"d\": enter debug mode.");
            Draw.AnyKey();
        }

        public static void Tie()
        {
            Console.Clear();
            Console.WriteLine("Its a tie!");
        }

        public static void Exit(State state) 
        {
            Console.Clear();
            Console.WriteLine("Thanks for pLaying!");
            Draw.AnyKey();
        }

        public static void Debug(State state) 
        {
            Console.WriteLine("----------");
            Console.WriteLine("Debug Mode");
            Draw.DebugCommands();
            Draw.InputPrompt();
        }
    }

    // public methods to log/output 
    public class Log 
    {
        public static void State(State state, string value) 
        {
            switch (value) {
                case "i":
                case "isactive":
                    Console.WriteLine("state.IsActive:");
                    Console.WriteLine($"{state.IsActive}");
                    break;
                case "t":
                case "topmessage":
                    Console.WriteLine("state.TopMessage:");
                    Console.WriteLine($"{state.TopMessage}");
                    break;
                case "c":
                case "currentplayer":
                    Console.WriteLine("state.CurrentPlayer:");
                    Console.WriteLine($"{state.CurrentPlayer}");
                    break;
                case "g":
                case "gameboard":
                    Console.WriteLine("state.GameBoard:");
                    Draw.DebugGameBoard(state);
                    break;
                case "w":
                case "wincheck":
                    Console.WriteLine("Logic.WinCheck:");
                    bool hasWon = Logic.WinCheck(state);
                    Console.WriteLine($"{hasWon}");
                    break;
                default:
                    Console.WriteLine($"Unknown state value \"{value}\".");
                    break;
            }
        }
    }
}