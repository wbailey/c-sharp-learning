﻿// See https://aka.ms/new-console-template for more information
// call class methods in constructor of parent to set props for child classes

namespace classes {
    class Program 
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello, World!");
            Parent parent = new Parent("TestStaging");
            // Console.WriteLine($"parent.IsTestStaging: {parent.IsTestStaging}");
            Child child = new Child("TestStaging");
            child.PrintEnvironment();
        }
    }
}
