﻿<%@ Page
    Title="Banner"
    Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="Banner.aspx.cs" 
    Inherits="cs_web_form_intro.Banner" 
    MasterPageFile="~/Site.Master"
%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <form id="form1" runat="server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:Button ID="btnGreet" runat="server" Text="Greet Me!" OnClick="btnGreet_Click" />
                <asp:Label ID="lblGreeting" runat="server" Text="" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGreet" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</asp:Content>
