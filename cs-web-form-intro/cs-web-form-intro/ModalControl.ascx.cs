﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cs_web_form_intro
{
    public partial class ModalControl : UserControl
        {
        public string ModalID { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            ModalID = "ModalControl_" + Guid.NewGuid().ToString("N");
            OpenModalButton.OnClientClick = $"{ModalID}_openModal(); return false;";
        }
    }
}