using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using Model;
using View;

namespace Controller
{
    // methods to modify the data model and state
    public class Dispatch
    {

        public static void CurrentState(State state, StateValue newState) 
        {
            state.CurrentState = newState;
        }

        public static void TopMessage(State state, string TopMessage) 
        {
           state.TopMessage = TopMessage;
        }

        public static void GameBoard(State state, int i, int j, string value) 
        {
            state.GameBoard[i, j] = value;
        }

        public static void CurrentPlayer(State state, string newPlayer)
        {
            state.CurrentPlayer = newPlayer;
        }

        public static void IsActive(State state, bool activeState)
        {
            state.IsActive = activeState;
        }
        // TODD: resets state to init values
        public static void Reset(State state)
        {
            state.IsActive = true;
            state.TopMessage = "";
            state.CurrentPlayer = "x";
            state.CurrentState = StateValue.Title;
            state.CurrentExecuteMoveMode = ExecuteMoveModes.Player;
            state.GameBoard = new string[3,3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    state.GameBoard[i, j] = "_";
                }
            }
        }
    }

    // logic that drives gameplay
    class Logic
    {
        public static bool ExecuteMovePlayer(State state, string player, string square)
        {
            string[] rowLabels = { "a", "b", "c" };
            Dictionary<string, int> rowMap = new Dictionary<string, int>();
            for (int index = 0; index < rowLabels.Length; index++) 
            {
                // rowMap.add(rowLabels[i], i);
                rowMap[rowLabels[index]] = index;
            }
            int i = rowMap[square[0].ToString()];
            int j = (int) char.GetNumericValue(square[1]) - 1;
            if (state.GameBoard[i, j] == "_")
            {
                Dispatch.GameBoard(state, i, j, player);
                return true;
            }
            return false;
        }

        public static bool ExecuteMoveComputerEasy(State state)
        {
            return true;
        }

        public static bool ExecuteMoveComputerHard(State state)
        {
            return true;
        }

        public static void ChangePlayer(State state) 
        {
            Dispatch.CurrentPlayer(state, state.CurrentPlayer == "x" ? "o" : "x");
        }

        public static bool WinCheck(State state)
        {
            int[,] winningCombos = {
                { 0, 1, 2 },
                { 3, 4, 5 },
                { 6, 7, 8 },
                
                { 0, 3, 6 },
                { 1, 4, 7 },
                { 2, 5, 8 },

                { 0, 4, 8 },
                { 6, 4, 2 },
            };
            string[] flattenedGameBoard = new string[10];
            int index = 0;
            foreach (string square in state.GameBoard)
            {
                flattenedGameBoard[index] = square;
                index++;
            }

            for (int i = 0; i < winningCombos.GetLength(0); i++) 
            {
                bool foundWin = true;
                for (int j = 0; j < winningCombos.GetLength(1); j++) 
                {
                    if (flattenedGameBoard[winningCombos[i, j]] !=  state.CurrentPlayer) 
                    {
                      foundWin = false;
                      break;
                    }
                }
                if (foundWin) 
                {
                    return foundWin;
                }
            }
            return false;
        }

        public static bool TieCheck(State state)
        {
            foreach(string square in state.GameBoard) 
            {
                if (square == "_")
                {
                    return false;
                }
            }
            return true;
        }
    }

    // public facing input handlers
    public class Input
    {
        public static string GetInput()
        {
            string? input = Console.ReadLine();
            if (input == null)
            {
                input = "";
            }
            return input;
        }

        public static void UpdateStateOnInput(State state, StateValue newState)
        {
            Input.GetInput();
            Dispatch.CurrentState(state, newState);
        }

        public static void ExitProgramOnInput(State state)
        {
            Input.GetInput();
            Dispatch.IsActive(state, false);
        }


        public static void HandleGamePlayInput(State state)
        {
            string input = Input.GetInput();
            string movePattern = @"^[A-Ca-c][123]$";
            string commandPattern = @"^[die]$";

            bool isValidMove = Regex.IsMatch(input, movePattern);
            bool isValidCommand = Regex.IsMatch(input, commandPattern);
            
            Dispatch.TopMessage(state, "");
            if (isValidCommand) {
                switch (input) {
                    case "d":
                        Dispatch.CurrentState(state, StateValue.Debug);
                        Logic.WinCheck(state);
                        break;
                    case "i":
                        Dispatch.CurrentState(state, StateValue.Info);
                        break;
                    case "e":
                        Dispatch.CurrentState(state, StateValue.Exit);
                        break;
                    default:
                        Dispatch.TopMessage(state, $"Unknown Command\"{input}\".");
                        break;
                }
            } 
            else if (isValidMove) 
            {
                if(!Logic.ExecuteMovePlayer(state, state.CurrentPlayer, input))
                {
                    Dispatch.TopMessage(state, $"Invalid move, try again");
                    return;
                }
                if(Logic.WinCheck(state)) 
                {
                    Dispatch.CurrentState(state, StateValue.Win);
                    return;
                } 
                else if (Logic.TieCheck(state)) 
                {
                    Dispatch.CurrentState(state, StateValue.Tie);
                    return;
                }
                 Logic.ChangePlayer(state);               
            } 
            else 
            {
                Dispatch.TopMessage(state, $"user input \"{input}\" is not valid. Use \"i\" for info on how to play.");
            }

        }
 
        public static void HandleDebugInput(State state) 
        {
            string input = Input.GetInput().ToLower();
            string debugPattern = @"^\b(isactive|topmessage|currentplayer|gameboard|wincheck|i|t|c|g|w)\b";
            string exitPattern = @"^\b(exit|e)\b";

            bool isValid = Regex.IsMatch(input, debugPattern);
            bool isValidExit = Regex.IsMatch(input, exitPattern);
            if (isValid) 
            {
                Log.State(state, input);
            } 
            else if (isValidExit) 
            {
                Dispatch.CurrentState(state, StateValue.GamePlay);
            } 
            else 
            {
                Console.WriteLine($"\"{input}\" not valid debug input.");
            }
        }
    }
}