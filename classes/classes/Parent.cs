namespace classes 
{
    class Parent
    {
        public string Server { get; set; }

        public bool IsTestStaging => Server == "TestStaging";
        public Parent(string server)
        {
            // Console.WriteLine("Hello From the Parent");
            Server = "";
            SetAppConfig(server);
        }
        // private -- this class only
        private void SetAppConfig(string server) 
        {
            Server = server;
        }
        // public -- any class
        public void PrintEnvironment()
        {
            Console.WriteLine($"Server: {Server}");
            Console.WriteLine($"IsTestStaging: {IsTestStaging}");
        }
        // protected this and derived classes
        protected void DoSomethingPrivate(string message)
        {
            Console.WriteLine(message);
        }
    } 
}