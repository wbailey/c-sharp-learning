﻿<%@ Page
    Title="RegistraionForm"
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="RegistrationForm.aspx.cs"
    Inherits="cs_web_form_intro.RegistrationForm" 
    MasterPageFile="~/Site.Master"
%>
<%@ Register Src="~/ModalControl.ascx" TagPrefix="uc" TagName="ModalControl" %>
<%@ Register Src="~/ReusableForm.ascx" TagPrefix="uc" TagName="ReusableForm" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="flex-row">
        <form id="form1" class="form">
            <div class="flex-col">
                <h2><%: Title %>:</h2>
                <asp:Label ID="LabelName" runat="server" Text="Name: "></asp:Label>
                <asp:TextBox ID="TextBoxName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorName"
                    runat="server"
                    ErrorMessage="Name is required"
                    ControlToValidate="TextBoxName"
                    ForeColor="Red">
                </asp:RequiredFieldValidator>

                <asp:Label ID="LabelEmail" runat="server" Text="Email: "></asp:Label>
                <asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2"
                    runat="server" ErrorMessage="Email is required"
                    ControlToValidate="TextBoxEmail"
                    ForeColor="Red">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="RegularExpressionValidator1"
                    runat="server"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ErrorMessage="Invalid email format"
                    ControlToValidate="TextBoxEmail"
                    ForeColor="Red">
                </asp:RegularExpressionValidator>

                <asp:Label runat="server" ID="PhoneLabel" Text="Phone:"></asp:Label>
                <asp:TextBox runat="server" ID="TextBoxPhone"></asp:TextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorPhone"
                    runat="server"
                    ErrorMessage="Phone is Required"
                    ControlToValidate="TextBoxPhone"
                    ForeColor="Red">
                </asp:RequiredFieldValidator>
                <%--
                <asp:RegularExpressionValidator
                    ID="RegularExpressionValidatorPhone"
                    runat="server"
                    ValidationExpression="/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/"
                    ErrorMessage="Invalid Phone Number Format"
                    ControlToValidate="TextBoxPhone"
                    ForeColor="Red">
                </asp:RegularExpressionValidator>
                --%>

                <uc:ReusableForm 
                    ID="ReusableForm1" 
                    runat="server" 
                    TextBoxLabel="Hello" />

                <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />

                <asp:Label ID="LabelMessage" runat="server"></asp:Label>
            </div>
        </form>
    </div>

    <uc:ModalControl ID="ModalControl1" runat="server" />
    <uc:ModalControl ID="ModalControl2" runat="server" />
</asp:Content>

