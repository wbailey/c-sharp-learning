namespace classes
{
    class Child : Parent
    {

        public Child(string server)
            : base(server)
        {
        }
        public void PrintEnvironment()
        {
            Console.WriteLine($"Server: {Server}");
            Console.WriteLine($"IsTestStaging: {IsTestStaging}");
        }
    }
}