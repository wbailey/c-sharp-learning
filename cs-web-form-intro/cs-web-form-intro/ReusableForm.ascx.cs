﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cs_web_form_intro
{
    public partial class ReusableForm : System.Web.UI.UserControl
    {
        public string TextBoxLabel
        {
            get { return Label1.Text; }
            set { Label1.Text = value; }
        }

        public string TextBoxValue
        {
            get { return TextBox1.Text; }
            set { TextBox1.Text = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}