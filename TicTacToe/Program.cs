﻿using Model;
using View;
using Controller;

/*
 * TODOS:
 * draw ASCII gameboard in view
 * handle input in controller
 * define state in model
 * handle state logic in controller
 * update views
 */

class Program
{
    static void Main(string[] args)
    {
        State state = new State();
        while (state.IsActive)
        {
            switch (state.CurrentState)
            {
                case StateValue.Title:
                    Render.Title();
                    Input.UpdateStateOnInput(state, StateValue.GamePlay);
                    break;
                case StateValue.GamePlay:
                    Render.GamePlay(state);
                    Input.HandleGamePlayInput(state);
                    break;
                case StateValue.Info:
                    Render.Info(state);
                    Input.UpdateStateOnInput(state, StateValue.GamePlay);
                    break;
                case StateValue.Win:
                    Render.Win(state);
                    Dispatch.Reset(state);
                    Input.UpdateStateOnInput(state, StateValue.Title);
                    break;
                case StateValue.Tie:
                    Render.Tie();
                    Dispatch.Reset(state);
                    Input.UpdateStateOnInput(state, StateValue.Title);
                    break;
                case StateValue.Exit:
                    Render.Exit(state);
                    Input.ExitProgramOnInput(state);
                    break;
                case StateValue.Debug:
                    Render.Debug(state);
                    Input.HandleDebugInput(state);
                    break;
                default:
                    Console.Write("Raise Error");
                    break;
            }
        }
    }
}