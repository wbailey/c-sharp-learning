namespace Model
{
    public enum StateValue
    {
        Title,
        GamePlay,
        Win,
        Tie,
        Info,
        Exit,
        Debug
    }

    public enum ExecuteMoveModes
    {
        Player,
        ComputerEasy,
        ComputerHard
    }

    public class State
    {
        public bool IsActive { get; set; }
        public string TopMessage { get; set; }
        public string CurrentPlayer { get; set; }
        public StateValue CurrentState { get; set; }
        public ExecuteMoveModes CurrentExecuteMoveMode { get; set; }
        public string[,] GameBoard { get; set; }

        public State()
        {
            IsActive = true;
            TopMessage = "";
            CurrentPlayer = "x";
            CurrentState = StateValue.Title;
            GameBoard = new string[3,3];
            CurrentExecuteMoveMode = ExecuteMoveModes.Player;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    GameBoard[i, j] = "_";
                }
            }
        }
    }
}