﻿using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

class Print
{
    // Generic type method
    // optional args
    public static void Array<T>(T[] array, string label = "")
    {
        if (!string.IsNullOrEmpty(label))
        {
            Console.Write($"{label} ");
        }
        foreach (T item in array)
        {
            Console.Write($"{item} ");
        }
        Console.Write("\n");
    }

    // Reflection -- runtime get variable types and all associated props and methods
    public static void TypeProperties<T>(T obj, string label = "")
    {
        if (!string.IsNullOrEmpty(label))
        {
            Console.WriteLine($"{label} ");
        }
        Type type = obj.GetType(); // typeof(T) for type literals 

        Console.WriteLine("Properties:");
        foreach (PropertyInfo prop in type.GetProperties())
        {
            Console.Write($"{prop} ");
        }
        Console.Write("\n");
        foreach (MethodInfo method in type.GetMethods())
        {
            Console.Write($"{method} ");
        }
        Console.Write("\n");
    }
}

class Program
{
    static void Main(string[] args)
    {
        // this is a comment
        /*
         * this is a multiline comment
         */
        /// -- this a an xml documentation comment TODO

        /*
         * OUPUT
         * use Console.Write (no newline) or Console.WriteLine() (with new line)
         */
        Console.Write("Hello, ");
        Console.Write("World!\n");
        Console.WriteLine("Hello, C#!");

        // Reflection
        // Print.TypeProperties("hello");
        int[] myArray= new int[10];
        Print.TypeProperties(myArray);


        // variable declaration -- camelCase is defualt
        // type varaibleNameCamelCase = assignment;
        int age = 36;
        const string name = "Weston Bailey";
        Console.WriteLine($"name: {name}, age {age}");
        // circle back to ToString method for output

        /*
         * types
         * https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/types
         * https://www.w3schools.com/cs/cs_data_types.php
         * VALUE TYPES -- stored directly in the stack
         * value types can be copied using the assignemnt operator
         * REFERNCE TYPES -- stored on the heap as a reference to the memory address
         * copied refernce types create another reference to the object at the same memory address
         */

        // Value types
        int myNum = 5;
        long myLong = 100L;
        double myDouble = 2.71828D;
        float myFloat = 3.14F;
        // chars use single quotes
        char myLetter = 'H';
        bool myBool = false;
        // strings use double quotes 
        string myText = "Hello!";
        Console.WriteLine($"myNum: {myNum}, myLong: {myLong}, myDouble: {myDouble}, myFloat: {myFloat}, myLetter: {myLetter}, myBool: {myBool}, myText: {myText}");

        // Type Casting Value types
        // implicit casting to larger types
        // char -> int -> long -> float -> double
        char myChar = 'W';
        double implicitCastDouble = myChar;
        Console.WriteLine($"Implicit Cast Double {implicitCastDouble}");
        // explicit casting from larger types/smaller types
        // double -> float -> long -> int -> char
        double doubleToCast = 87D;
        char explicitCastChar = (char)doubleToCast;
        Console.WriteLine($"explicitCastChar {explicitCastChar}");
        // does it work the other way?
        char castLargerChar = 'B';
        double explicitLargerDouble = (double)castLargerChar;
        Console.WriteLine($"explicitLargerDouble {explicitLargerDouble}");
        // language builtins for type casting using Convert.Type()
        // Convert.ToBoolean, Convert.ToDouble, Convert.ToString, Convert.ToInt32 (int), Convert.ToInt64 (long)
        char convertedChar = Convert.ToChar(111L);
        Console.WriteLine(convertedChar);

        /*
         * TODO: STRINGS
         */

        // TODO: formatting
        // TODO: concatentation
        // TODO: methods

        /*
         * REFERENCE TYPES
         */

        // Arrays
        // https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/arrays
        // Array syntax: <datatype>[] variableName = new <datatype>[size]
        int[] myInts = new int[10];
        for (int i = 0; i < myInts.Length; i++)
        {
            myInts[i] = i * 100;
        }

        for (int i = 0; i < myInts.Length; i++)
        {
            Console.Write($"{myInts[i]} ");
            Console.Write("\n");
        }

        long[] myLongs = { 0L, 1L, 2L, 3L, 5L, 6L, 7L, 8L, 9L };

        for (int i = 0; i < myLongs.Length; i++)
        {
            Console.WriteLine($"{myLongs[i]}");
        }

        string[] fruits = new string[10];
        fruits = ["banana", "orange", "grape", "pomegranite"];
        foreach (string fruit in fruits)
        {
            Console.WriteLine(fruit);
        }

        // Multi-dimensional Arrays
        int[,] twoDimensionInts = new int[10, 10];
        int rows = twoDimensionInts.GetLength(0);
        int cols = twoDimensionInts.GetLength(1);
        Console.WriteLine($"there are {twoDimensionInts.Length} total items in twoDimensionalInts");
        ; for (int i = 0; i < rows; i++)
        {
            Console.Write($"row[{i}]:");
            for (int j = 0; j < cols; j++)
            {
                Console.Write($" {j}");
                twoDimensionInts[i, j] = i * j;
            }
            Console.Write("\n");
        }

        int newCol = 0;
        foreach (int number in twoDimensionInts)
        {
            if (newCol == 0)
            {
                Console.Write("\n");
            }
            Console.Write($"{number} ");
            newCol++;
            newCol = newCol % rows;
        }
        Console.Write("\n");

        // declare and set values
        string[,] stringMatrix =
        {
            { "hello", "goodbye" },
            { "apple", "peach" },
            { "javascript",  "c#" }
        };

        foreach (string word in stringMatrix)
        {
            Console.WriteLine(word);
        }

        // jagged arrays are 2d (nd?) of different nested array size
        int[][] jaggedInts = new int[10][];

        for (int i = 0; i < jaggedInts.Length; i++)
        {
            jaggedInts[i] = new int[i + 1];
        }

        for (int i = 0; i < jaggedInts.Length; i++)
        {
            for (int j = 0; j < jaggedInts[i].Length; j++)
            {
                jaggedInts[i][j] = i * j;
            }
        }

        for (int i = 0; i < jaggedInts.Length; i++)
        {
           for (int j = 0; j < jaggedInts[i].Length; j++)
            {
                Console.Write($"{jaggedInts[i][j]} ");
            }
            Console.Write("\n");
        }

        // Can jagged arrays also be all of the same size? CGPT says they are slower and use more memory than 2d arrays :/
        string[][] jaggedStrings = new string[10][];
        for (int i = 0; i < jaggedStrings.Length; i++)
        {
            jaggedStrings[i] = new string[10];
        }

        for (int i = 0; i < jaggedStrings.Length; i++)
        {   
            for (int j = 0; j < jaggedStrings[i].Length; j++)
            {
                jaggedStrings[i][j] = $"[{i}, {j}] "; 
            }
        }

        for (int i = 0; i < jaggedStrings.Length; i++)
        {
            for (int j = 0; j < jaggedStrings[i].Length; j++)
            {
                Console.Write(jaggedStrings[i][j]);
            }
            Console.Write("\n");
        }

        // Array methods and properties
        // get amount of items in an Array + dimensions
        // .Length .Rank, .GetLength()
        int[] testArray = new int[10];
        for (int i = 0; i < testArray.Length; i++)
        {
            testArray[i] = i;
        }
        Console.WriteLine($"testArray.Length: {testArray.Length}");

        int[,] test2dArray = new int[5, 10];
        int[] dimensionLengths = new int[test2dArray.Rank];
        for (int i = 0; i < dimensionLengths.Length; i++)
        {
            dimensionLengths[i] = test2dArray.GetLength(i);
        }
        Console.WriteLine($"test2dArray rows: {dimensionLengths[0]}, cols: {dimensionLengths[1]}, test2dArray.Length {test2dArray.Length}, test2dArray.Rank {test2dArray.Rank}");

        // copying Arrays
        // copy of ref
        int[] testArrayRefCopy = testArray;
        testArray[5] = 100000;
        Print.Array(testArrayRefCopy, "testArrayRefCopy");
        // deep copy
        int[] testArrayCopy = new int[testArray.Length];
        Array.Copy(testArray, testArrayCopy, testArray.Length);
        for (int i = 0; i < testArrayCopy.Length; i++)
        {
            testArrayCopy[i] *= 5;
        }
        Print.Array(testArrayCopy, "testArrayCopy");
        Print.Array(testArray, "testArray");
        // Array.Clone makes a shallow copy and if the array includes referncese they are ref copys not deep copies

        // Array.
        // TODO: Lists
        // TODO: Stacks/Queues
        // TODO: Dictionary/Hash map
        // TODO: Hash Set
        // TODO: Read Only Collections
        // TODO: Tuples

        /*
         * TODO: OPERATORS
         */

    }
}